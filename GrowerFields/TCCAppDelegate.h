//
//  TCCAppDelegate.h
//  GrowerFields
//
//  Created by Richard Shin on 4/27/14.
//  Copyright (c) 2014 Richard Shin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
