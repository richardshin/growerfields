//
//  TCCCommunicator.h
//  GrowerFields
//
//  Created by Richard Shin on 5/23/14.
//  Copyright (c) 2014 Richard Shin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCCField.h"
#import "TCCFieldDetails.h"

@interface TCCCommunicator : NSObject

/**
 Performs a synchronous fetch of field JSON data and returns @c NSArray of @c TCCField objects.
 */
- (NSArray *)fetchAllFields;

/**
 Performs an asychronous fetch of field details JSON data. Executes @c completionHandler block when
 finished -- but *NOT* guaranteed to be on the main thread!
 @param field Field to fetch details of
 @param completionHandler Block to execute when finished. Not guaranteed to be executed on main thread.
 */
- (void)fetchDetailsForField:(TCCField *)field onCompletion:(void(^)(TCCFieldDetails *details, NSError *error))completionHandler;

@end
