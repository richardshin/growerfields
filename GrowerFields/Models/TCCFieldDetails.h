//
//  TCCFieldDetails.h
//  GrowerFields
//
//  Created by Richard Shin on 4/27/14.
//  Copyright (c) 2014 Richard Shin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCCField.h"

@interface TCCFieldDetails : NSObject

@property (strong, nonatomic) NSNumber *seventyTwoHourPrecipitation;

@end
