//
//  TCCCommunicator.m
//  GrowerFields
//
//  Created by Richard Shin on 5/23/14.
//  Copyright (c) 2014 Richard Shin. All rights reserved.
//

#import "TCCCommunicator.h"

@implementation TCCCommunicator

NSString * const FieldsURL = @"https://qa1-twi.climate.com/apps/mobile/v2/users/25415/fields.json?mock=true";
NSString * const FieldDetailsURL = @"https://qa1-twi.climate.com/apps/mobile/v2/users/25415/fields/%@.json?mock=true"; // %@ should be Field ID

- (NSArray *)fetchAllFields
{
    NSURL *url = [NSURL URLWithString:FieldsURL];
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    if (!data) return nil;
    
    NSDictionary *JSONDictionary; // TODO: create a JSON dictionary from data!
    
    NSMutableArray *fields = [NSMutableArray array];
    for (NSDictionary *fieldDictionary in [JSONDictionary valueForKeyPath:@"data"]) {
        TCCField *field = [[TCCField alloc] init];
        field.id = fieldDictionary[@"id"];
        field.name = fieldDictionary[@"name"];
        field.acreage = fieldDictionary[@"acreage"];
        field.twentyFourHourPrecipitation = [fieldDictionary valueForKeyPath:@"last_twenty_four_hours.inches"];
        field.thumbnailURL = [NSURL URLWithString:fieldDictionary[@"thumbnail"]];

        [fields addObject:field];
    }
    
    return [fields copy];
}

- (void)fetchDetailsForField:(TCCField *)field onCompletion:(void(^)(TCCFieldDetails *details, NSError *error))completionHandler
{
    // TODO: how do I make this code execute asynchronously?
    
    NSURL *url = [NSURL URLWithString:FieldDetailsURL];
    // Question: Why can we pass options as 0?
    NSData *data = [NSData dataWithContentsOfURL:url options:0 error:NULL];
    
    if (!data) {
        // TODO: Call completion handler
    }
    
    NSDictionary *JSONDictionary; // TODO: create a JSON dictionary from data!

    TCCFieldDetails *fieldDetails = [[TCCFieldDetails alloc] init];
    fieldDetails.seventyTwoHourPrecipitation = [JSONDictionary valueForKeyPath:@"data.last_seventy_two_hours.inches"];
    
    // TODO: Call completion handler!
}

@end
