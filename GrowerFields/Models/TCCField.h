//
//  TCCField.h
//  GrowerFields
//
//  Created by Richard Shin on 4/27/14.
//  Copyright (c) 2014 Richard Shin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCCField : NSObject

@property (strong, nonatomic) NSNumber *id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *acreage;
@property (strong, nonatomic) NSString *twentyFourHourPrecipitation;
@property (strong, nonatomic) NSURL *thumbnailURL;

- (NSString *)fieldPrecipitationDescription;

@end
