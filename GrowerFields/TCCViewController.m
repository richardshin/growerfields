//
//  TCCViewController.m
//  GrowerFields
//
//  Created by Richard Shin on 4/27/14.
//  Copyright (c) 2014 Richard Shin. All rights reserved.
//

#import "TCCViewController.h"
#import "TCCField.h"
#import "TCCFieldTableViewCell.h"

@interface TCCViewController ()
@property (strong, nonatomic) NSArray *fields; // of TCCField
@end

@implementation TCCViewController

NSString * const fieldCellIdentifier = @"TCCFieldTableViewCell";

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.communicator = [[TCCCommunicator alloc] init];
    self.fields = [self.communicator fetchAllFields];
}

#pragma mark - Protocol conformance

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.fields count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:fieldCellIdentifier];
    
    if (cell) {
        // TODO: need to configure this cell...
    }
    
    return cell;
}

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 81.0;
}

@end
