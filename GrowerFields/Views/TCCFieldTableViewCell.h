//
//  TCCFieldTableViewCell.h
//  GrowerFields
//
//  Created by Richard Shin on 4/27/14.
//  Copyright (c) 2014 Richard Shin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCCField.h"
#import "TCCFieldDetails.h"

@interface TCCFieldTableViewCell : UITableViewCell

@property (strong, nonatomic) TCCField *field;

@property (strong, nonatomic) UIImage *fieldThumbnailImage;

@end
