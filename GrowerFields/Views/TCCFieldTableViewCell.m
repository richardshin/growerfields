//
//  TCCFieldTableViewCell.m
//  GrowerFields
//
//  Created by Richard Shin on 4/27/14.
//  Copyright (c) 2014 Richard Shin. All rights reserved.
//

#import "TCCFieldTableViewCell.h"

@interface TCCFieldTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UILabel *acresLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *fieldThumbnailImageView;

@end

@implementation TCCFieldTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Custom accessors

- (void)setField:(TCCField *)field
{
    self.nameLabel.text = field.name;
    [self.idLabel setText:[field.id stringValue]];
    self.acresLabel.text = field.acreage;
    self.temperatureDescriptionLabel.text = [field fieldPrecipitationDescription];
}

- (void)setFieldThumbnailImage:(UIImage *)fieldThumbnailImage
{
    self.fieldThumbnailImageView.image = fieldThumbnailImage;
}

@end
