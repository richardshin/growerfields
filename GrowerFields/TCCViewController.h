//
//  TCCViewController.h
//  GrowerFields
//
//  Created by Richard Shin on 4/27/14.
//  Copyright (c) 2014 Richard Shin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCCCommunicator.h"

@interface TCCViewController : UITableViewController

@property (strong, nonatomic) TCCCommunicator *communicator;

@end
