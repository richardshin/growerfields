********************
*** INTRODUCTION ***
********************

Joe Bloggs has started writing an app that displays fields using a RESTful API to receive JSON data.
He's pretty okay at programming, but he's asked for your help and there's a lot of room for improvement.
Can you help him implement the following?


****************
*** FEATURES ***
****************

1. Fields list that displays name, ID, acres, temperature description, and image thumbnail for each field.
   The thumbnail should be loaded on a background thread to keep the UI responsive.

2. An on-off slider that toggles the display of fields from using TCCField to using TCCFieldDetail
   to display the detail temperature description.

3. Tapping a cell presents another view controller with a push segue. This second view controller needs
   to display an editable text field populated with the field name. Changing the field name and pressing
   a "Save" button should navigate back to the field list and display the updated field name.


**********************
*** CONSIDERATIONS ***
**********************

1. You may refactor the code as you see fit. There are a few places where it makes a lot of sense to do so!

2. Some of the code that's been provided may be buggy or inconsistent. Use it as scaffolding, but don't
   trust it.

2. TCCField and TCCFieldDetail have a lot of overlap in data, but for very important unspecified reasons,
   they should remain separate classes.